package com.devcamp.task6070.order.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070.order.api.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder,Long> {
    
}
